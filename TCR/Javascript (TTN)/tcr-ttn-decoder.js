

/**
 * TCR Payload Decoders
 *
 * THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 *   
*/

// Array of all available device types (since JavaScript has no enum)
const DeviceTypes = [
    "TCR-LS",         /* 0 Low  Speed       (2 Speed Groups) */
    "TCR-LSS",        /* 1 Low  Speed Solar (2 Speed Groups) */
    "TCR-HS",         /* 2 High Speed       (4 Speed Groups) */
    "TCR-HSS",        /* 3 High Speed Solar (4 Speed Groups) */
    "TCR-LSA",        /* 4 Low  Speed AC Powered (2 Speed Groups) */
    "TCR-LSB",        /* 5 Low  Speed AC External Battery Powered (4 Speed Groups) */
    "TCR-HSA",        /* 6 High Speed AC Powered (4 Speed Groups) */
    "TCR-HSB",        /* 7 High Speed External Battery Powered (4 Speed Groups) */
    "TCR-LSBS",       /* 8 Low Speed External Battery (External) + Solar (2 Speed Groups) */
    "TCR-HSBS",       /* 9 High Speed External Battery (External) + Solar (4 Speed Groups) */
    "TCR-DLI",        /* 10 DC powered, LoRaWAN, Internal Antenna */    
    "TCR-DLE",        /* 11 DC powered, LoRaWAN, External Antenna */
    "TCR-SLI",        /* 12 Solar powered, LoRaWAN, Internal Antenna */
    "TCR-SLE",        /* 13 Solar powered, LoRaWAN, External Antenna */    
];

// Array of speed available classes (TCR GEN2)
const SpeedClassTypes = [
    "P",           /* 0 Speed class for people counting (very low speed) */
    "LS",          /* 1 Low speed traffic counting */
    "HS"           /* 2 High speed traffic counting */
];

// Array of all available config keys (since JavaScript has no enum)
// Use this array to have a key instead of just an index number
const ConfigKeys = [

    // Cat P
    "00",                   /** 00 */
    "cat_p_enabled",        /** 01 Enable category */
    "cat_p_set_ltr",        /** 02 Overwrite LTR counter of category */
    "cat_p_set_rtl",        /** 03 Overwrite RTL counter of category */
    "cat_p_min_size",       /** 04 min object size of category */
    "cat_p_max_size",       /** 05 max object size of category */
    "cat_p_min_speed",      /** 06 min object speed of category */
    "cat_p_max_speed",      /** 07 max object speed of category */
    "08",                   /** 08 */
    "09",                   /** 09 */
    "0a",                   /** 0A */
    "0b",                   /** 0B */
    "0c",                   /** 0C */
    "0d",                   /** 0D */
    "0e",                   /** 0E */
    "0f",                   /** 0F */

    // Cat A
    "10",                   /** 10 */
    "cat_a_enabled",        /** 11 Enable category */
    "cat_a_set_ltr",        /** 12 Overwrite LTR counter of category */
    "cat_a_set_rtl",        /** 13 Overwrite RTL counter of category */
    "cat_a_min_size",       /** 14 min object size of category */
    "cat_a_max_size",       /** 15 max object size of category */
    "cat_a_min_speed",      /** 16 min object speed of category */
    "cat_a_max_speed",      /** 17 max object speed of category */
    "18",                   /** 18 */
    "19",                   /** 19 */
    "1a",                   /** 1A */
    "1b",                   /** 1B */
    "1c",                   /** 1C */
    "1d",                   /** 1D */
    "1e",                   /** 1E */
    "1f",                   /** 1F */

    // Cat B
    "20",                   /** 20 */
    "cat_b_enabled",        /** 21 Enable category */
    "cat_b_set_ltr",        /** 22 Overwrite LTR counter of category */
    "cat_b_set_rtl",        /** 23 Overwrite RTL counter of category */
    "cat_b_min_size",       /** 24 min object size of category */
    "cat_b_max_size",       /** 25 max object size of category */
    "cat_b_min_speed",      /** 26 min object speed of category */
    "cat_b_max_speed",      /** 27 max object speed of category */
    "28",                   /** 28 */
    "29",                   /** 29 */
    "2a",                   /** 2A */
    "2b",                   /** 2B */
    "2c",                   /** 2C */
    "2d",                   /** 2D */
    "2e",                   /** 2E */
    "2f",                   /** 2F */

    // Cat C
    "30",                   /** 30 */
    "cat_c_enabled",        /** 31 Enable category */
    "cat_c_set_ltr",        /** 32 Overwrite LTR counter of category */
    "cat_c_set_rtl",        /** 33 Overwrite RTL counter of category */
    "cat_c_min_size",       /** 34 min object size of category */
    "cat_c_max_size",       /** 35 max object size of category */
    "cat_c_min_speed",      /** 36 min object speed of category */
    "cat_c_max_speed",      /** 37 max object speed of category */
    "38",                   /** 38 */
    "39",                   /** 39 */
    "3a",                   /** 3A */
    "3b",                   /** 3B */
    "3c",                   /** 3C */
    "3d",                   /** 3D */
    "3e",                   /** 3E */
    "3f",                   /** 3F */

    /* Application Settings */
    "40",                   /** 40 */
    "mode",                 /** 41 operation mode */
    "holdoff",              /** 42 holdoff timer in seconds  */
    "timeout",              /** 43 auto-zero timeout in minutes  */
    "sumup",                /** 44 totalizer instead of interval counter  */
    "fallbackcat",          /** 45 fallback category  */
    "46",                   /** 46 */
    "47",                   /** 47 */
    "48",                   /** 48 */
    "49",                   /** 49 */
    "4a",                   /** 4A */
    "4b",                   /** 4B */
    "4c",                   /** 4C */
    "4d",                   /** 4D */
    "4e",                   /** 4E */
    "4f",                   /** 4F */

    /* Radar Settings */
    "50",                   /** 50 */
    "radar_enabled",        /** 51 radar module on / off */
    "radar_channel",        /** 52 Radar Channel: 1, 2 */
    "radar_sens",           /** 53 Radar Sensitivity Level [%] */
    "radar_beam",           /** 54 Virtual Radar Beam Angle [°] */
    "radar_dir",            /** 55 Virtual Radar Beam Direction (+ = right, - = to the left) [°] */
    "radar_ltrdist",        /** 56 Minimal distance to object from left [cm] */
    "radar_rtldist",        /** 57 Minimal distance to object from right [cm] */
    "radar_autotune",       /** 58 Autotune, 0 = off , 1 = on */
    "59",                   /** 59  */
    "5a",                   /** 5A  */
    "5b",                   /** 5B  */
    "5c",                   /** 5C  */
    "5d",                   /** 5D  */
    "5e",                   /** 5E  */
    "5f",                   /** 5F  */

    /* LoRaWAN Settings */
    "60",                   /** 60 */
    "lora_interval",        /** 61 LoRa Uplink interval in [min] */
    "lora_class",           /** 62 LoRaWAN Device class 0: Class A, 1= Class C */
    "lora_confirmed",       /** 63 confirmed uplinks mode */
];

function app_payload_v1_decoder(bytes, port) {
    var obj = {};

    if (port != 15) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 15 for application payloads.";
        return obj;
    }

    if (bytes.length != 32) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V1 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01) {

        obj.sbx_batt = bytes[3];                        // battery gauge when equiped with an SBX solar charger 0…100%
        obj.sbx_pv = (bytes[4] << 8) | (bytes[5]);      // Solar panel power when equiped with SBX 0…65535 mW

        var temp = (bytes[6] << 8) | (bytes[7]);
        temp = bin16dec(temp);
        obj.temp = Math.floor(temp / 10);               // CPU Temperature

        obj.l0_cnt = (bytes[8] << 8) | (bytes[9]);      // object count from left in speed class 0, 0-65535
        obj.l0_avg = bytes[10];                         // average speed from left in speed class 0, 0-65535
        obj.r0_cnt = (bytes[11] << 8) | (bytes[12]);    // object count from right in speed class 0, 0-65535
        obj.R0_AVG = bytes[13];                         // average speed from right in speed class 0, 0-65535

        obj.l1_cnt = (bytes[14] << 8) | (bytes[15]);    // object count from left in speed class 1, 0-65535
        obj.l1_avg = bytes[16];                         // average speed from left in speed class 1, 0-65535
        obj.r1_cnt = (bytes[17] << 8) | (bytes[18]);    // object count from right in speed class 1, 0-65535
        obj.r1_avg = bytes[19];                         // average speed from right in speed class 1, 0-65535

        obj.l2_cnt = (bytes[20] << 8) | (bytes[21]);    // object count from left in speed class 1, 0-65535
        obj.l2_avg = bytes[22];                         // average speed from left in speed class 1, 0-65535
        obj.r2_cnt = (bytes[23] << 8) | (bytes[24]);    // object count from right in speed class 1, 0-65535
        obj.r2_avg = bytes[25];                         // average speed from right in speed class 1, 0-65535

        obj.l3_cnt = (bytes[26] << 8) | (bytes[27]);    // object count from left in speed class 1, 0-65535
        obj.l3_avg = bytes[28];                         // average speed from left in speed class 1, 0-65535
        obj.r3_cnt = (bytes[29] << 8) | (bytes[30]);    // object count from right in speed class 1, 0-65535
        obj.r3_avg = bytes[31];                         // average speed from right in speed class 1, 0-65535
    }
    else {
        obj.error = "ERROR: TCR application payload V1 should start with be0201..  ";
    }
    return obj;
}

function app_payload_v2_decoder(bytes, port) {
    var obj = {};

    if (port != 15) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 15 for application payloads.";
        return obj;
    }

    if (bytes.length != 33) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V2 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02) {

        obj.sbx_batt = (bytes[3] << 8) | (bytes[4]);    // Battery voltage when equiped with an SBX solar charger 0-65535mV
        obj.sbx_pv = (bytes[5] << 8) | (bytes[6]);      // Solar panel power when equiped with SBX 0…65535 mW

        var temp = (bytes[7] << 8) | (bytes[8]);
        temp = bin16dec(temp);
        obj.temp = Math.floor(temp / 10);               // CPU Temperature

        obj.l0_cnt = (bytes[9] << 8) | (bytes[10]);      // object count from left in speed class 0, 0-65535
        obj.l0_avg = bytes[11];                         // average speed from left in speed class 0, 0-65535
        obj.r0_cnt = (bytes[12] << 8) | (bytes[13]);    // object count from right in speed class 0, 0-65535
        obj.r0_avg = bytes[14];                         // average speed from right in speed class 0, 0-65535

        obj.l1_cnt = (bytes[15] << 8) | (bytes[16]);    // object count from left in speed class 1, 0-65535
        obj.l1_avg = bytes[17];                         // average speed from left in speed class 1, 0-65535
        obj.r1_cnt = (bytes[18] << 8) | (bytes[19]);    // object count from right in speed class 1, 0-65535
        obj.r1_avg = bytes[20];                         // average speed from right in speed class 1, 0-65535

        obj.l2_cnt = (bytes[21] << 8) | (bytes[22]);    // object count from left in speed class 1, 0-65535
        obj.l2_avg = bytes[23];                         // average speed from left in speed class 1, 0-65535
        obj.r2_cnt = (bytes[24] << 8) | (bytes[25]);    // object count from right in speed class 1, 0-65535
        obj.r2_avg = bytes[26];                         // average speed from right in speed class 1, 0-65535

        obj.l3_cnt = (bytes[27] << 8) | (bytes[28]);    // object count from left in speed class 1, 0-65535
        obj.l3_avg = bytes[29];                         // average speed from left in speed class 1, 0-65535
        obj.r3_cnt = (bytes[30] << 8) | (bytes[31]);    // object count from right in speed class 1, 0-65535
        obj.r3_avg = bytes[32];                         // average speed from right in speed class 1, 0-65535
    }
    else {
        obj.error = "ERROR: TCR application payload V2 should start with be0202..  ";
    }
    return obj;
}

function counting_payload_V1_decoder(bytes, port) {
    var obj = {};

    if (port == 14) // P Counter (former known as speed class 0)
    {
        obj.data0_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l0_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 0, 0-65535
        obj.l0_avg = bytes[5];                          // average speed from left in speed class 0, 0-65535
        obj.r0_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 0, 0-65535
        obj.r0_avg = bytes[8];                          // average speed from right in speed class 0, 0-65535
    }

    if (port == 15) // A Counter (former known as speed class 1)
    {
        obj.data1_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l1_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 1, 0-65535
        obj.l1_avg = bytes[5];                          // average speed from left in speed class 1, 0-65535
        obj.r1_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 1, 0-65535
        obj.r1_avg = bytes[8];                          // average speed from right in speed class 1, 0-65535
    }

    if (port == 16) // B Counter (former known as speed class 2)
    {
        obj.data2_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l2_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 2, 0-65535
        obj.l2_avg = bytes[5];                          // average speed from left in speed class 2, 0-65535
        obj.r2_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 2, 0-65535
        obj.r2_avg = bytes[8];                          // average speed from right in speed class 2, 0-65535
    }

    if (port == 17) // C Counter (former known as speed class 3)
    {
        obj.data3_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l3_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 3, 0-65535
        obj.l3_avg = bytes[5];                          // average speed from left in speed class 3, 0-65535
        obj.r3_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 3, 0-65535
        obj.r3_avg = bytes[8];                          // average speed from right in speed class 3, 0-65535
    }

    obj.sbx_batt = bytes[9] * 100;    // Battery voltage when equipped with an SBX solar charger( *100 to get mV) 
    return obj;
}

function counting_payload_V2_decoder(bytes, port) {
    var obj = {};

    if (port == 14) // P Counter (former known as speed class 0)
    {
        obj.data0_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l0_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 0, 0-65535
        obj.l0_avg = bytes[5];                          // average speed from left in speed class 0, 0-65535
        obj.r0_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 0, 0-65535
        obj.r0_avg = bytes[8];                          // average speed from right in speed class 0, 0-65535
    }

    if (port == 15) // A Counter (former known as speed class 1)
    {
        obj.data1_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l1_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 1, 0-65535
        obj.l1_avg = bytes[5];                          // average speed from left in speed class 1, 0-65535
        obj.r1_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 1, 0-65535
        obj.r1_avg = bytes[8];                          // average speed from right in speed class 1, 0-65535
    }

    if (port == 16) // B Counter (former known as speed class 2)
    {
        obj.data2_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l2_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 2, 0-65535
        obj.l2_avg = bytes[5];                          // average speed from left in speed class 2, 0-65535
        obj.r2_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 2, 0-65535
        obj.r2_avg = bytes[8];                          // average speed from right in speed class 2, 0-65535
    }

    if (port == 17) // C Counter (former known as speed class 3)
    {
        obj.data3_timestamp = (bytes[1] << 8) | (bytes[2]);  // HH MM of last data packet 
        obj.l3_cnt = (bytes[3] << 8) | (bytes[4]);      // object count from left in speed class 3, 0-65535
        obj.l3_avg = bytes[5];                          // average speed from left in speed class 3, 0-65535
        obj.r3_cnt = (bytes[6] << 8) | (bytes[7]);      // object count from right in speed class 3, 0-65535
        obj.r3_avg = bytes[8];                          // average speed from right in speed class 3, 0-65535
    }

    obj.voltage = bytes[9] * 100;                       // power supply voltage (dc or solar)
    return obj;
}

function config_payload_v1_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.";
        return obj;
    }

    if (bytes.length != 27) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V1 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01) {

        obj.type = DeviceTypes[bytes[3]];                      // 00: TCR, 01: TCR-S

        obj.fw_version = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version

        obj.mode = bytes[7];                   // 00: Timespan, 01: Trigger

        obj.class = bytes[8];                     // 00: Class A, 02: Class C

        obj.confirmed = bytes[9];                     // 00: Uncofirmed, 01: Confirmed  

        obj.interval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes

        obj.lci = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes

        obj.holdoff = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes

        obj.sens = bytes[16];                          // 00: Uncofirmed, 01: Confirmed

        obj.ltrdist = bytes[17];                                  // Distance to lane with traffic from left

        obj.rtldist = bytes[18];                               // Distance to lane with traffic from right

        obj.sc0_start = bytes[19];                                 // Speed class 0 window start 0-255 km/h
        obj.scO_edn = bytes[20];                                   // Speed class 0 window end 0-255 km/h

        obj.sc1_start = bytes[21];                                 // Speed class 1 window start 0-255 km/h
        obj.sc1_end = bytes[22];                                   // Speed class 1 window end 0-255 km/h

        obj.sc2_start = bytes[23];                                 // Speed class 2 window start 0-255 km/h
        obj.sc2_end = bytes[24];                                   // Speed class 2 window end 0-255 km/h

        obj.sc3_start = bytes[25];                                 // Speed class 3 window start 0-255 km/h
        obj.sc3_end = bytes[26];                                   // Speed class 3 window end 0-255 km/h
    }
    else {
        obj.error = "ERROR: TCR configuration payload V1 should start with be0201..  ";
    }
    return obj;
}

function config_payload_v2_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.";
        return obj;
    }

    if (bytes.length != 29) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V2 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02) {

        obj.type = DeviceTypes[bytes[3]];                      // 00: TCR-LS, 01: TCR-LSS

        obj.fw_version = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version

        obj.mode = bytes[7];                   // 00: Timespan, 01: Trigger

        obj.class = bytes[8];                     // 00: Class A, 02: Class C

        obj.confirmed = bytes[9];                     // 00: Uncofirmed, 01: Confirmed

        obj.interval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes

        obj.lci = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes

        obj.holdoff = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes

        obj.sens = bytes[16];                          // 00: Unconfirmed, 01: Confirmed

        obj.ltrdist = (bytes[17] << 8) | (bytes[18]);          // Distance to lane with traffic from left

        obj.rtldist = (bytes[19] << 8) | (bytes[20]);          // Distance to lane with traffic from right

        obj.scO_start = bytes[21];                                 // Speed class 0 window start 0-255 km/h
        obj.scO_end = bytes[22];                                   // Speed class 0 window end 0-255 km/h

        obj.sc1_start = bytes[23];                                 // Speed class 1 window start 0-255 km/h
        obj.sc1_end = bytes[24];                                   // Speed class 1 window end 0-255 km/h

        obj.sc2_start = bytes[25];                                 // Speed class 2 window start 0-255 km/h
        obj.sc2_end = bytes[26];                                   // Speed class 2 window end 0-255 km/h

        obj.sc3_start = bytes[27];                                 // Speed class 3 window start 0-255 km/h
        obj.sc3_end = bytes[28];                                   // Speed class 3 window end 0-255 km/h

    }
    else {
        obj.error = "ERROR: TCR configuration payload V2 should start with be0202..  ";
    }
    return obj;
}

function config_payload_v3_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.";
        return obj;
    }

    if (bytes.length != 33) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V3 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x03) {

        obj.type = DeviceTypes[bytes[3]];                      // 00: TCR-LS, 01: TCR-LSS

        obj.fw_version = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version

        obj.mode = bytes[7];                   // 00: Timespan, 01: Trigger

        obj.class = bytes[8];                     // 00: Class A, 02: Class C

        obj.confirmed = bytes[9];                     // 00: Uncofirmed, 01: Confirmed

        obj.interval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes

        obj.lci = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes

        obj.holdoff = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes

        obj.autotune = bytes[16];                           // 00: Autotuning off, 01: Autotuning active

        obj.sens = bytes[17];                          // 00: Uncofirmed, 01: Confirmed

        obj.ltrdist = (bytes[18] << 8) | (bytes[19]);          // Distance to lane with traffic from left

        obj.rtldist = (bytes[20] << 8) | (bytes[21]);          // Distance to lane with traffic from right

        obj.scO_start = bytes[22];                                 // Speed class 0 window start 0-255 km/h
        obj.scO_end = bytes[23];                                   // Speed class 0 window end 0-255 km/h

        obj.sc1_start = bytes[24];                                 // Speed class 1 window start 0-255 km/h
        obj.sc1_end = bytes[25];                                   // Speed class 1 window end 0-255 km/h

        obj.sc2_start = bytes[26];                                 // Speed class 2 window start 0-255 km/h
        obj.sc2_end = bytes[27];                                   // Speed class 2 window end 0-255 km/h

        obj.sc3_start = bytes[28];                                 // Speed class 3 window start 0-255 km/h
        obj.sc3_end = bytes[29];                                   // Speed class 3 window end 0-255 km/h

        obj.sbx_fw = bytes[30] + "." + bytes[31] + "." + bytes[32];  // SBX Solar Charger Firmware Version 

    }
    else {
        obj.error = "ERROR: TCR configuration payload V3 should start with be0203..  ";
    }
    return obj;
}

function config_payload_v4_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        obj.error = "ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.";
        return obj;
    }

    if (bytes.length != 42) {
        obj.error = "ERROR: Wrong payload length";
        return obj;
    }

    // check for Parametric TCR V3 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x04) {

        obj.type = DeviceTypes[bytes[3]];                      // 00: TCR-LS, 01: TCR-LSS , ...

        obj.fw_version = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version

        obj.mode = bytes[7];                   // 00: Timespan, 01: Trigger

        obj.class = bytes[8];                     // 00: Class A, 02: Class C

        obj.confirmed = bytes[9];                     // 00: Uncofirmed, 01: Confirmed

        obj.interval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes

        obj.lci = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes

        obj.holdoff = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Seconds

        obj.radar_enabled = bytes[16];                              // 00: Radar Module is disabled, 01: is enabled

        obj.beam = bytes[17];                                 // 30-80° Radar Detection Angle

        obj.mindist = (bytes[18] << 8) | (bytes[19]);              // Min Distance to Target in 100-1000 cm 

        obj.maxdist = (bytes[20] << 8) | (bytes[21]);              // Max Distance to Target in 100-1000 cm  

        obj.minspeed = bytes[22];                                  // Min Detection Speed 1-MaxSpeed km/h  

        obj.maxspeed = bytes[23];                                  // Max Detection Speed 1-MaxSpeed km/h  

        obj.autotune = bytes[24];                           // 00: Autotuning off, 01: Autotuning active

        obj.sens = bytes[25];                          // 00: Uncofirmed, 01: Confirmed

        obj.scO_start = bytes[26];                                 // Speed class 0 window start 0-255 km/h
        obj.scO_end = bytes[27];                                   // Speed class 0 window end 0-255 km/h

        obj.sc1_start = bytes[28];                                 // Speed class 1 window start 0-255 km/h
        obj.sc1_end = bytes[29];                                   // Speed class 1 window end 0-255 km/h

        obj.sc2_start = bytes[30];                                 // Speed class 2 window start 0-255 km/h
        obj.sc2_end = bytes[31];                                   // Speed class 2 window end 0-255 km/h

        obj.sc3_start = bytes[32];                                 // Speed class 3 window start 0-255 km/h
        obj.sc3_end = bytes[33];                                   // Speed class 3 window end 0-255 km/

        obj.ltrdist = (bytes[34] << 8) | (bytes[35]);          // Distance to lane with traffic from left

        obj.rtldist = (bytes[36] << 8) | (bytes[37]);          // Distance to lane with traffic from right

        obj.sbx_fw = bytes[38] + "." + bytes[39] + "." + bytes[40];  // SBX Solar Charger Firmware Version 

        obj.channel = bytes[41];                              // Radar Channel, 0 = Low Frequency (Default), 1 = High Frequency

    }
    else {
        obj.error = "ERROR: TCR configuration payload V4 should start with be0204..  ";
    }
    return obj;
}

function device_id_v1_decoder(bytes, port) {
    var obj = {};
    obj.type = DeviceTypes[bytes[2]];                                                // 00: TCR-LS, 01: TCR-LSS , ...
    obj.fw_version = (bytes[5] & 0xf0) / 0x10 + "." + (bytes[5] & 0x0f) + "." + bytes[6];  // Firmware Major Version
    obj.sbx_fw = (bytes[7] & 0xf0) / 0x10 + "." + (bytes[7] & 0x0f) + "." + bytes[8];  // SBX Solar Charger Firmware Version     
    return obj;
}

function device_id_v2_decoder(bytes, port) {
    var obj = {};
    obj.type = DeviceTypes[bytes[2]];                                                // 00: TCR-LS, 01: TCR-LSS , ...
    obj.speedclass = SpeedClassTypes[bytes[4]];                                      // 00: VLS, 01: LS , 02: HS
    obj.fw_version = (bytes[6] & 0xf0) / 0x10 + "." + (bytes[6] & 0x0f) + "." + bytes[7];  // Firmware Major Version
    obj.sbx_fw = (bytes[8] & 0xf0) / 0x10 + "." + (bytes[8] & 0x0f) + "." + bytes[9];  // SBX Solar Charger Firmware Version     
    return obj;
}

function config_payload_V1_decoder(bytes, port) {
    var obj = {};

    // 8 Bit values
    if(bytes.length == 3) 
    {
        obj[ConfigKeys[bytes[1]]] = bytes[2];    
    }

    // 16 Bit values
    if(bytes.length >= 4) 
    {
        obj[ConfigKeys[bytes[1]]] = (bytes[2] << 8) | (bytes[3]);     
    }
    return obj;
}

function bin16dec(bin) {
    var num = bin & 0xFFFF;
    if (0x8000 & num) num = -(0x010000 - num);
    return num;
}

function hexToBytes(hex) {
    hex = hex.replace(/\s/g, "");
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

 
// TTN Entry Point
function decodeUplink(input) {
    var bytes = input.bytes;
    var port = input.fPort;
    var obj = {};
    if(port == 15 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01)
    {         
        // it's an Application Payload V1
        obj = app_payload_v1_decoder(bytes, port);
    }

    if(port == 15 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02)
    {         
        // it's an Application Payload V2
        obj = app_payload_v2_decoder(bytes, port);
    }

    if(port >= 14 && port <=17 && bytes[0] == 0xa1 )
    {         
        // it's a Counting Payload V1 (TCR V2.0 )
        obj = counting_payload_V1_decoder(bytes, port);
    }

    if(port >= 14 && port <=17 && bytes[0] == 0xa2 )
    {         
        // it's a Counting Payload V2 (TCR V2.2 )
        obj = counting_payload_V2_decoder(bytes, port);
    } 

    if(port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01)
    {
        // it's a Configuration Payload V1
        obj = config_payload_v1_decoder(bytes, port);
    }

    if(port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02)
    {
        // it's a Configuration Payload V2
        obj = config_payload_v2_decoder(bytes, port);
    }
    
    if(port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x03)
    {
        // it's a Configuration Payload V3
        obj = config_payload_v3_decoder(bytes, port);
    }

    if(port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x04)
    {
        // it's a Configuration Payload V4
        obj = config_payload_v4_decoder(bytes, port);
    }

    if(port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[3] == 0xd1)
    {
        // it's a Device ID Payload V1
        obj = device_id_v1_decoder(bytes, port);
    }

    if (port == 190 && bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[3] == 0xd2) 
    {
        // it's a Device ID Payload V2
        obj = device_id_v2_decoder(bytes, port);
    }

    if (port == 1 && bytes[0] == 0xc1) {
        // it's a Config Payload Response (Uplinked ACK of a Config Downlink) V1 (TCR V2.0)
        obj = config_payload_V1_decoder(bytes, port);
    }
    
    return {
        data: obj,
        errors: obj.error
    }
};