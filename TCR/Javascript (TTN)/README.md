# TCR Uplink Payload formatter example for TTN  

Use this coder as a _Custom Javascript formatter_ and modify it to your needs.

## Installation

1. Open TTN Console
2. Open a TCR end Device
3. Open the _Payload formatters_ tab
4. Paste the content of the file __tcr-ttn-decoder.js__ into the _Formater code_ box.
5. Save changes

![](tcr_ttn_decoder.png)

## Test the decoder

You can use the "Payload formatter" tab of individual end devices to test uplink payload formatters and to define individual payload formatter settings per end device.
Past following uplink payloads into the _Byte payload_ field. Also add the port number.
See [Payload Documentation](https://docs.parametric-analytics.com/tcr/manuals/lora_payload/) for more info.

### DeviceID Payload (Port 190)

 ```be 02 00 d1 00 21 04 42 01``` 
 
 Decodes to ...
 
```json
{
  "type": "TCR-LS",
  "fw_version": "2.1.4",
  "sbx_fw": "4.2.1"
}
```


<br>

### Counter Payload  P (Port 14)

 ```a1 13 14 00 01 01 00 02 04 4e``` 
 
 Decodes to ...
 
```json
{
  "l0_cnt": 1,
  "l0_avg": 1,
  "r0_cnt": 2,
  "r0_avg": 4,
  "sbx_batt": 7800
}
```

<br>

### Counter Payload A (Port 15)

 ```a1 13 14 00 10 16 00 20 18 4f``` 
 
 Decodes to ...
 
```json
{
  "l1_cnt": 16,
  "l1_avg": 22,
  "r1_cnt": 32,
  "r1_avg": 24,
  "sbx_batt": 7900
}
```

<br>

### Counter Payload B (Port 16)

 ```a1 13 14 00 01 01 00 02 04 4d``` 
 
 Decodes to ...
 
```json
{
  "l2_cnt": 1,
  "l2_avg": 1,
  "r2_cnt": 2,
  "r2_avg": 4,
  "sbx_batt": 7700
}
```

<br>

### Counter Payload C (Port 17)

 ```a1 13 14 00 01 01 00 02 04 4c``` 
 
 Decodes to ...
 
```json
{
  "l3_cnt": 1,
  "l3_avg": 1,
  "r3_cnt": 2,
  "r3_avg": 4,
  "sbx_batt": 7600
}
```