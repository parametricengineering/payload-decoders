
var decoder = require("./tcr_payload_decoder.js");

// test the TCR application payload decoder (v1)
console.log("TCR Application Payload V1");
console.log(JSON.stringify(decoder.decode('be02016412c218b800000000010600000000020b00000000011e000000000000', 15), null, 2));
/*

{
  "SBX_BATT": 100,
  "SBX_PV": 4802,
  "TEMP": 632,
  "L0_CNT": 0,
  "L0_AVG": 0,
  "R0_CNT": 1,
  "R0_AVG": 6,
  "L1_CNT": 0,
  "L1_AVG": 0,
  "R1_CNT": 2,
  "R1_AVG": 11,
  "L2_CNT": 0,
  "L2_AVG": 0,
  "R2_CNT": 1,
  "R2_AVG": 30,
  "L3_CNT": 0,
  "L3_AVG": 0,
  "R3_CNT": 0,
  "R3_AVG": 0
}

*/

// test the TCR application payload decoder (v2)
console.log("TCR Application Payload V2");
console.log(JSON.stringify(decoder.decode('be02021cc0000000a0000108000000000000000000000000000000000000000000', 15), null, 2));
/*

{
   "SBX_BATT": 7360,
  "SBX_PV": 0,
  "TEMP": 16,
  "L0_CNT": 1,
  "L0_AVG": 8,
  "R0_CNT": 0,
  "R0_AVG": 0,
  "L1_CNT": 0,
  "L1_AVG": 0,
  "R1_CNT": 0,
  "R1_AVG": 0,
  "L2_CNT": 0,
  "L2_AVG": 0,
  "R2_CNT": 0,
  "R2_AVG": 0,
  "L3_CNT": 0,
  "L3_AVG": 0,
  "R3_CNT": 0,
  "R3_AVG": 0
}

*/


// test the TCR config payload decoder (v1)
console.log("TCR Config Payload V1");
console.log(JSON.stringify(decoder.decode('be020100010000000000000305a0000064050f010708191a313278', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.0.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 0,
  "UplinkInterval": 3,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarSensitivity": 100,
  "LTRLaneDist": 5,
  "RTLLaneDist": 15,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 25,
  "SC2_START": 26,
  "SC2_END": 49,
  "SC3_START": 50,
  "SC3_END": 120
}
*/

// test the TCR config payload decoder (v2)
console.log("TCR Config Payload V2");
console.log(JSON.stringify(decoder.decode('be020200010201000000000a05a000006400fa01c2010708191a313278', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.2.1",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 0,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarSensitivity": 100,
  "LTRLaneDist": 250,
  "RTLLaneDist": 450,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 25,
  "SC2_START": 26,
  "SC2_END": 49,
  "SC3_START": 50,
  "SC3_END": 120
}
*/


// test the TCR config payload decoder (v3)
console.log("TCR Config Payload V3");
console.log(JSON.stringify(decoder.decode('be020300010300000001000a05a00000005a00fa00fa0107082800000000040100', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.3.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarAutotuning": 0,
  "RadarSensitivity": 90,
  "LTRLaneDist": 250,
  "RTLLaneDist": 250,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 40,
  "SC2_START": 0,
  "SC2_END": 0,
  "SC3_START": 0,
  "SC3_END": 0,
  "SBX_Firmware": "4.1.0"
}
*/

// test the TCR config payload decoder (v4)
console.log("TCR Config Payload V4");
console.log(JSON.stringify(decoder.decode('be 02 04 00 01 04 00 00 00 01 00 0a 05 a0 00 00 00 50 00 64 01 90 01 28 00 5a 01 07 08 28 00 00 00 00 00 fa 00 fa 00 00 00 00', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.3.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarAutotuning": 0,
  "RadarSensitivity": 90,
  "LTRLaneDist": 250,
  "RTLLaneDist": 250,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 40,
  "SC2_START": 0,
  "SC2_END": 0,
  "SC3_START": 0,
  "SC3_END": 0,
  "SBX_Firmware": "4.1.0"
}
*/

// test the TCR Device ID V1 Payload Decoder 
console.log("TCR DeviceID V1 Payload");
console.log(JSON.stringify(decoder.decode('be 02 00 d1 00 20 08 42 01', 190), null, 2));
/*
{
  "type": "TCR-LS",
  "fw_version": "2.0.8",
  "sbx_fw": "4.2.1"
}
*/

// test the TCR Device ID V2 Payload Decoder 
console.log("TCR DeviceID V2 Payload");
console.log(JSON.stringify(decoder.decode('be 02 0a d2 01 00 21 00 42 01', 190), null, 2));
/*
{
  "type": "TCR-DLI",
  "speedclass": "LS",
  "fw_version": "2.1.0",
  "sbx_fw": "4.2.1"
}
*/

// test the TCR Counter Payload 0 (P)
console.log("TCR TCR Counter Cat 0 Payload");
console.log(JSON.stringify(decoder.decode('a2 13 14 00 01 01 00 02 04 4e', 14), null, 2));
/*
{
  "l0_cnt": 1,
  "l0_avg": 1,
  "r0_cnt": 2,
  "r0_avg": 4,
  "sbx_batt": 7800
}
*/

// test the TCR Counter Payload 1 (A)
console.log("TCR Counter Cat 1 Payload");
console.log(JSON.stringify(decoder.decode('a2 13 14 00 10 16 00 20 18 4f', 15), null, 2));
/*
{
  "l1_cnt": 16,
  "l1_avg": 22,
  "r1_cnt": 32,
  "r1_avg": 24,
  "sbx_batt": 7900
}
*/

// test the TCR Counter Payload 2 (B)
console.log("TCR Counter Cat 2 Payload");
console.log(JSON.stringify(decoder.decode('a2 13 14 00 01 01 00 02 04 4d', 16), null, 2));
/*
{
  "l2_cnt": 1,
  "l2_avg": 1,
  "r2_cnt": 2,
  "r2_avg": 4,
  "sbx_batt": 7700
}
*/

// test the TCR Counter Payload 3 (C)
console.log("TCR Counter Cat 3 Payload ");
console.log(JSON.stringify(decoder.decode('a2 13 14 00 01 01 00 02 04 4c', 17), null, 2));
/*
{
  "l3_cnt": 1,
  "l3_avg": 1,
  "r3_cnt": 2,
  "r3_avg": 4,
  "sbx_batt": 7600
}
*/

// Test complete settings list (see https://docs.parametric.ch/tcr/manuals/lora_payload/#config-downlinks-port-1)
console.log("Test settings list");

// Cat P 
console.log(JSON.stringify(decoder.decode('c1010001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1040001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1050064', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1060001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1070007', 1), null, 2));

// Cat A 
console.log(JSON.stringify(decoder.decode('c1110001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1140064', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c11500c8', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1160005', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1170028', 1), null, 2));

// Cat B 
console.log(JSON.stringify(decoder.decode('c1210001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c12400fa', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1250258', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c126000a', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1270064', 1), null, 2));

// Cat C 
console.log(JSON.stringify(decoder.decode('c1310001', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1340258', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c13503e8', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c136000a', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1370050', 1), null, 2));

// Application Settings
console.log(JSON.stringify(decoder.decode('c1410000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1420000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1430000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1440000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1450002', 1), null, 2));

// Radar Settings
console.log(JSON.stringify(decoder.decode('c1510000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1520002', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c153005f', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1540046', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1550000', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c15601c2', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c15600fa', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1580001', 1), null, 2));

// LoRaWAN Settings
console.log(JSON.stringify(decoder.decode('c161000a', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1620002', 1), null, 2));
console.log(JSON.stringify(decoder.decode('c1630000', 1), null, 2));

// Test index function 
console.log(decoder.getIndexOfCommand("cat_p_enabled")) // should be 1
console.log(decoder.getIndexOfCommand("nonono")) // should be -1
