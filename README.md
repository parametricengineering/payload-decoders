![Parametric GmbH](https://www.parametric.ch/assets/images/logos/logo.png)

# Payload Decoders for Parametric LoRaWAN Devices

These code samples are provided free of charge as a starting point to write your own decoders.

| Device | Languages |
|--------|-----------|
| **PCR2**   | [NodeJS](https://bitbucket.org/parametricengineering/payload-decoders/src/master/PCR2/NodeJS/)  |
| **TCR**    |  [NodeJS](https://bitbucket.org/parametricengineering/payload-decoders/src/master/TCR/NodeJS/)|


Contribution to this repository is highly welcome.

