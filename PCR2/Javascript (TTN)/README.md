# PCR2 Uplink Payload Formatter Example for TTN

Use this coder as a _Custom Javascript formatter_ and modify it to your needs.

## Installation

1. Open TTN Console
2. Open a PCR2 end Device
3. Open the _Payload formatters_ tab
4. Paste the content of the file __pcr2-decoder.js__ into the _Formater code_ box.
5. Save changes

![](ttn_pl_decoder.png)

## Test the decoder

You can use the "Payload formatter" tab of individual end devices to test uplink payload formatters and to define individual payload formatter settings per end device.
Past following uplink payloads into the _Byte payload_ field. Also add the port number.
See [Payload Documentation](https://docs.parametric-analytics.com/pcr2/manuals/lora_payload/) for more info.


### DeviceID Payload (Port 190)

 ```be 01 00 d5 04 00 00``` 
 
 Decodes to ...
 
```json
{
  "DevType": "PCR2-IN",
  "Firmware": "4.0.0"
}
```


<br>

### AppData Payload (Port 14)

 ```be 01 05 a5 00 0a 00 0b 4e 10``` 
 
 Decodes to ...
 
```json
{
  "DCNT": 16,
  "DevType": "PCR2-ODS",
  "LTR": 10,
  "RTL": 11,
  "SBX_BATT": 7800
}
```

