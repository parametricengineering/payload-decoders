
var decoder = require("./pcr2_payload_decoder.js");

// test the ELSYS app pl decoder
console.log("ELSYS Application Payload");
console.log(JSON.stringify(decoder.decode('0a0001160002010023',14), null, 2));

// test the extended app pl v3
console.log("Extended Application Payload V3");
console.log(JSON.stringify(decoder.decode('be01030001000200030004640ce40139',14), null, 2));

// test the extended app pl v4
console.log("Extended Application Payload V4");
console.log(JSON.stringify(decoder.decode('be010400010002000300041c200ce40139',14), null, 2));

// test the extended app pl v5
console.log("Application Payload V5 (DeviceType = PCR2-IN, LTR=1, RTL=2, SBXBatt=8.1V ");
console.log(JSON.stringify(decoder.decode('be0101a50000000000e8',14), null, 2));


// test the config pl v2
console.log("Config Payload V2");
console.log(JSON.stringify(decoder.decode('00030200000000000a05a0000050',190), null, 2))

// test the config pl v3
console.log("Config Payload V3");
console.log(JSON.stringify(decoder.decode('be01030003040000000001000a05a00000000050',190), null, 2))

// test the config pl v4
console.log("Config Payload V4");
console.log(JSON.stringify(decoder.decode('be01040403060003020000000a05a00064000100b4005a50003201f4011450',190), null, 2))

// test the config pl v5
console.log("Config Payload V5");
console.log(JSON.stringify(decoder.decode('be010500030a0100020001000a05a00000000000780128003200c80107005a040101',190), null, 2))
/*
Config Payload V5
{
  "DeviceType": 0,
  "Firmware": "3.10.1",
  "OperationMode": 0,
  "PayloadType": 2,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "CapacityLimit": 0,
  "HoldoffTime": 0,
  "InactivityTimeout": 120,
  "RadarEnabled": 1,
  "BeamAngle": 40,
  "MinDist": 50,
  "MaxDist": 200,
  "MinSpeed": 1,
  "MaxSpeed": 7,
  "RadarAutotune": 0,
  "RadarSensitivity": 90,
  "SBXVersion": "4.1.1"
}
*/
console.log("Config Payload V6");
console.log(JSON.stringify(decoder.decode('be010605030c0400020000000a05a0000000000078012800320bb80107003c04020000',190), null, 2))
console.log(JSON.stringify(decoder.decode ('be010600030c0400020001000205a0000000000078011e003200320101000a00000000',190), null, 2))
console.log(JSON.stringify(decoder.decode('be010600030c0100020001000305a00000000000780128003200c80107003c01020301',190), null, 2)) 

console.log("DeviceID Payload");
console.log(JSON.stringify(decoder.decode('be 01 00 d5 00 40 04 42 00',190), null, 2))

console.log("AppData Payload");
console.log(JSON.stringify(decoder.decode('be 01 05 a5 00 0a 00 0b 4e 10',14), null, 2))



// Test complete settings list (see https://docs.parametric.ch/pcr2/manuals/lora_payload/#port_1)
console.log("Test settings list");

// Application Settings
console.log(JSON.stringify(decoder.decode('be0100c50100', 1), null, 2));    // "mode": 0
console.log(JSON.stringify(decoder.decode('be0100c502000a', 1), null, 2));  // "interval": 10   
console.log(JSON.stringify(decoder.decode('be0100c5030001', 1), null, 2));  // "holdoff": 1
console.log(JSON.stringify(decoder.decode('be0100c50403e8', 1), null, 2));  // "caplim": 1000
console.log(JSON.stringify(decoder.decode('be0100c50500b4', 1), null, 2));  // "timeout": 180
console.log(JSON.stringify(decoder.decode('be0100c50601', 1), null, 2));    // "sumup": 1
console.log(JSON.stringify(decoder.decode('be0100c51001', 1), null, 2));    // "radar_enabled": 1
console.log(JSON.stringify(decoder.decode('be0100c51102', 1), null, 2));    // "radar_channel": 2
console.log(JSON.stringify(decoder.decode('be0100c5125a', 1), null, 2));    // "radar_sens": 90
console.log(JSON.stringify(decoder.decode('be0100c51350', 1), null, 2));    // "radar_beam": 80
console.log(JSON.stringify(decoder.decode('be0100c514f6', 1), null, 2));    // "radar_dir": 246
console.log(JSON.stringify(decoder.decode('be0100c5150064', 1), null, 2));  // "radar_mindist": 100
console.log(JSON.stringify(decoder.decode('be0100c51601f4', 1), null, 2));  // "radar_maxdist": 500
console.log(JSON.stringify(decoder.decode('be0100c51701', 1), null, 2));    // "radar_autotune": 1
console.log(JSON.stringify(decoder.decode('be0100c56300', 1), null, 2));    // "lora_confirmed": 0

// Test index function 
console.log(decoder.getIndexOfCommand("radar_sens")) // should be 18
console.log(decoder.getIndexOfCommand("nonono")) // should be -1